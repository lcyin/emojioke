import * as passport from 'passport';
import * as passportJWT from 'passport-jwt';
import {jwtConfig} from './jwt';
import { userService } from './main';

const  JWTStrategy = passportJWT.Strategy;
const {ExtractJwt} = passportJWT;

passport.use(new JWTStrategy({
        secretOrKey: jwtConfig.jwtSecret,
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    },async (payload,done)=>{
        const user = (await userService.getUsers())
                .find((user:any)=>user.id == payload.id);
        if(user){
            return done(null,{ id : user.id,
                level: user.level
            });
        } else {
            return done(new Error("User not Found"),null);
        }
    })
);

passport.serializeUser(function(user:any, done) {
    done(null, user.id);
});

passport.deserializeUser(async function(id, done) {
    const Users = await userService.getUsers()
    const user = Users.find((user:any)=> id == user.id);
    if(user){
        return done(null,user);
    }else{
        return done(new Error("User not Found"));
    }
}); 