export interface User {
    username:string,
    password:string,
    email:string,
    
}

export interface Song{
    songname: string
    singername: string;
    genre: string
}

export interface Emoji{
    type: string;
    value: number;
    filename: string
}

export interface Comments {
    songname: string;
    username: string;
    comments: string;
}

export interface SongRecord {
    username: string;
    songname: string;
    date: string;
    filename: string
}

export interface FavouriteSongRecord{
    user: string;
    songRecord: string
}