import * as express from 'express';
import * as bodyParser from 'body-parser';
import expressSession = require('express-session');
import passport = require('passport');
import * as cors from 'cors';


import * as Knex from 'knex';
import knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])


import { LoginRouter } from './Router/LoginRouter';
import { SignupService } from './Service/SignupService';
import { SignupRouter } from './Router/SignupRouter';
import { UserService } from './Service/UserService';
import { UserRouter } from './Router/UserRouter';
import { SingerRouter } from './Router/SingerRouter';
import { SingerService } from './Service/SingerService';
import { SongRecordService } from './Service/SongRecordService';
import { SongService } from './Service/SongService';
import { SongRecordRouter } from './Router/SongRecordRouter';
import { SongRouter } from './Router/SongRouter';
import { FavouriteSongRecordService } from './Service/FavouriteSongRecordService';
import { FavouriteUserService } from './Service/FavouriteUserService';
import { EmojiRouter } from './Router/EmojiRouter';
import { EmojiService } from './Service/EmojiService';
import { FavouriteSongRecordRouter } from './Router/FavoriteSongRecordRouter';
import { FavouriteUserRouter } from './Router/FavoriteUserRouter';
import { CommentRouter } from './Router/CommentRouter';
import { CommentService } from './Service/CommentsService';
import { RecordEmojiService } from './Service/RecordEmojiService';
import { RecordEmojiRouter } from './Router/RecordEmojiRouter';
import { UploadRouter } from './Router/UploadRouter'
import { UploadService } from './Service/UploadService'



const signupService = new SignupService(knex)
export const userService = new UserService(knex)
const singerService = new SingerService(knex)
const songService = new SongService(knex)
export const songRecordService = new SongRecordService(knex);
const favourRecordService = new FavouriteSongRecordService(knex)
const favourUserService = new FavouriteUserService(knex)
const commentService = new CommentService(knex)
const emojiService = new EmojiService(knex)
const recordEmojiService = new RecordEmojiService(knex)
const uploadService = new UploadService()


const loginRouter = new LoginRouter(userService, signupService)
const signupRouter = new SignupRouter(signupService)
const userRouter = new UserRouter(userService)
const singerRouter = new SingerRouter(singerService)
const songRouter = new SongRouter(songService)
const songRecordRouter = new SongRecordRouter(songRecordService);
const favourRecordRouter = new FavouriteSongRecordRouter(favourRecordService)
const favourUserRouter = new FavouriteUserRouter(favourUserService)
const commentRouter = new CommentRouter(commentService)
const emojiRouter = new EmojiRouter(emojiService)
const recordEmojiRouter = new RecordEmojiRouter(recordEmojiService)
const uploadRouter = new UploadRouter(uploadService)

const app = express();
app.use(cors())
app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());
import './passport'


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('assets'))
app.use(express.static('uploads'))

const isLoggedIn = passport.authenticate('jwt', { session: false });

app.use('/login', loginRouter.router());
app.use('/signup', signupRouter.router());
app.use('/users',isLoggedIn, userRouter.router());
app.use('/singer', singerRouter.router());
app.use('/song', songRouter.router());
app.use('/songRecord', songRecordRouter.router());
app.use('/emoji', emojiRouter.router());
app.use('/favourRecord', isLoggedIn, favourRecordRouter.router());
app.use('/favourUser', isLoggedIn, favourUserRouter.router());
app.use('/comment', commentRouter.router());
app.use('/recordEmoji', recordEmojiRouter.router())
app.use('/uploads',isLoggedIn, uploadRouter.router())

const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});

