import * as Knex from "knex";

exports.up = async function (knex: Knex) {
    const hasTable = await knex.schema.hasTable('song_records');
    if(!hasTable){
        return knex.schema.createTable('song_records',(table)=>{
            table.increments();
            table.integer("user_id").unsigned()
            table.integer("song_id").unsigned()
            table.foreign("user_id").references('users.id');
            table.foreign("song_id").references('songs.id');
            table.string("filename");
            table.timestamps(false,true);
        });  
    }else{
        return Promise.resolve();
    }
};

exports.down = function (knex: Knex) {
    return knex.schema.dropTableIfExists('song_records');
};
