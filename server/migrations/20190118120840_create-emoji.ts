import * as Knex from "knex";

exports.up = async function (knex: Knex) {
    const hasTable = await knex.schema.hasTable('emoji');
    if(!hasTable){
        return knex.schema.createTable('emoji',(table)=>{
            table.increments();
            table.enu('type', ['very_good', 'good', 'neutral', 'bad', 'very_bad']);
            table.enu("value", [1.0, 0.75, 0.5, 0.25, 0]);
            table.string("filename");
        });  
    }else{
        return Promise.resolve();
    }
};

exports.down = function (knex: Knex) {
    return knex.schema.dropTableIfExists('emoji');
};
