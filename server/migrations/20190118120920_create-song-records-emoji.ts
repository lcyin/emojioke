import * as Knex from "knex";

exports.up = async function (knex: Knex) {
    const hasTable = await knex.schema.hasTable('song_records_emoji');
    if(!hasTable){
        return knex.schema.createTable('song_records_emoji',(table)=>{
            table.increments();
            table.integer("user_id").unsigned();
            table.integer("emoji_id").unsigned();
            table.integer("song_record_id").unsigned()
            table.foreign("user_id").references('users.id');
            table.foreign("emoji_id").references('emoji.id');
            table.foreign("song_record_id").references('song_records.id');
            table.float('clicked_time');
            table.float("clicked_coord_x");
            table.float("clicked_coord_y");
        });  
    }else{
        return Promise.resolve();
    }
};

exports.down = function (knex: Knex) {
    return knex.schema.dropTableIfExists('song_records_emoji');
};
