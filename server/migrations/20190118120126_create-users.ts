import * as Knex from "knex";

exports.up = async function (knex: Knex) {
    const hasTable = await knex.schema.hasTable('users');
    if(!hasTable){
        return knex.schema.createTable('users',(table)=>{
            table.increments();
            table.string("username");
            table.string("password").notNullable();
            table.string("email").notNullable();
            table.string("profile_pic");
            table.string("level")
            table.timestamps(false,true);
        });  
    }else{
        return Promise.resolve();
    }
};

exports.down = function (knex: Knex) {
    return knex.schema.dropTableIfExists('users');
};
