import * as Knex from "knex";

exports.up = async function (knex: Knex) {
    const hasTable = await knex.schema.hasTable('singer');
    if(!hasTable){
        return knex.schema.createTable('singer',(table)=>{
            table.increments();
            table.string("name").notNullable();
            table.enu('gender', ['Male', 'Female']);
        });  
    }else{
        return Promise.resolve();
    }
};

exports.down = function (knex: Knex) {
    return knex.schema.dropTableIfExists('singer');
};
