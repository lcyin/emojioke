import * as Knex from "knex";

exports.up = async function (knex: Knex) {
    const hasTable = await knex.schema.hasTable('favourite_users');
    if(!hasTable){
        return knex.schema.createTable('favourite_users',(table)=>{
            table.increments();
            table.integer("user_id").unsigned();
            table.integer("followed_user_id").unsigned();
            table.foreign("user_id").references("users.id");
            table.foreign("followed_user_id").references("users.id")
        });  
    }else{
        return Promise.resolve();
    }
};

exports.down = function (knex: Knex) {
    return knex.schema.dropTableIfExists('favourite_users');
};
