import * as express from 'express';
import { FavouriteUserService } from '../Service/FavouriteUserService';

export class FavouriteUserRouter {
    constructor(private favouriteUserService: FavouriteUserService) {
    }

    router() {
        const router = express.Router();
        router.get('/', this.getFavouriteUser.bind(this))
        router.post('/:id', this.addFavouriteUser.bind(this));
        router.delete('/:id', this.deleteFavouriteUser.bind(this));
        return router;
    }

    async getFavouriteUser(req: express.Request, res: express.Response) {
        const user = await this.favouriteUserService.getUser()
        res.json(user)
    }

    async addFavouriteUser(req: express.Request, res: express.Response) {

        if (req.params.id == null || req.body.followedUserId == null) {
            res.status(500)
            res.json({ result: "empty input" })
            return
        }

        try {

            const newFollowedUser = {
                userId: req.params.id,
                followedUserId: req.body.followedUserId
            }

            const result = await this.favouriteUserService.addUser(newFollowedUser)

            if (result[0] > 0){
                res.json({ result: 'add success' })
            }

        } catch (e) {
            res.status(500)
            res.json({ result: 'add failed' })
        }

    }

    async deleteFavouriteUser(req: express.Request, res: express.Response) {
        const result = this.favouriteUserService.deleteUser(req.params.id);
        result.then((data) => {
            if (data > 0) {
                res.json({ result: 'delete 1' })
            } else {
                res.status(500)
                res.json({ result: 'favourite user not exist' })
            }
        })
    }
}