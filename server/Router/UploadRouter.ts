import * as express from 'express';
import * as multer from 'multer';
import { UploadService } from '../Service/UploadService';
import { songRecordService } from '../main'

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/../uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})

const upload = multer({ storage })

export class UploadRouter {
    constructor(private uploadService: UploadService) {

    }


    router() {
        const router = express.Router();
        router.post('/', upload.single('mp3'), this.uploadRecord.bind(this));
        return router;
    }

    async uploadRecord(req: express.Request, res: express.Response) {
        console.log(req.file)
        const result = await this.uploadService.convertVideo(req.file)
        console.log('vvvvvvvvvvv uploadRecord')
        console.log(result)

            if (result.isSuccess) {
                const user_id = Number(req.user.id)
                const song_id = Number(req.body.song_id)
                const filename = result.filename

                const newUploadRecord: any = {
                    user_id: user_id,
                    song_id: song_id,
                    filename: filename
                }
                // update song record to database
                console.log(newUploadRecord)
                const databaseResult = await songRecordService.addSongRecord(newUploadRecord)
                if (databaseResult[0] != null) {
                    res.json({ isSuccess: 1, result: 'add record success' });
                } else {
                    res.status(500)
                    res.json({ result: 'add record failed' })
                }

            }

    }

}