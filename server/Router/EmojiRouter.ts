import * as express from 'express';
import { EmojiService } from '../Service/EmojiService';
// import { Emoji } from '../interface';

export class EmojiRouter {
    constructor(private emojiService: EmojiService) {
    }

    router() {
        const router = express.Router();
        router.get('/', this.getEmoji.bind(this))
        router.post('/:id', this.getEmojiById.bind(this));
        router.post('/', this.addEmoji.bind(this));
        return router;
    }

    async getEmoji(req: express.Request, res: express.Response) {
        const emoji = await this.emojiService.getEmoji()
        res.json({ isSuccess: 1, data: emoji })
    }

    async getEmojiById(req: express.Request, res: express.Response) {
        const emoji = await this.emojiService.getEmojiById(req.params.id)
        if (emoji[0] != null) {
            res.json(emoji)
        } else {
            res.status(500);
            res.json({ result: 'emoji_not_found' });
        }
    }

    async addEmoji(req: express.Request, res: express.Response) {
        if (req.body.type == '' || req.body.value == null || req.body.filename == '') {
            res.status(500)
            res.json({ result: "empty input" })
            return
        }

        try {
            const newEmoji = {
                type: req.body.type,
                value: req.body.value,
                filename: req.body.filename
            }
            const result = await this.emojiService.addEmoji(newEmoji)
            if (result[0] > 0) {
                res.json({ result: "add success" })
            }
        } catch (e) {
            if (e) {
                res.status(500)
                res.json({ result: 'add failed' })
            }
        }

    }


}