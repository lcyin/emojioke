import * as express from 'express';
import {SongService} from '../Service/SongService';

export class SongRouter {
    constructor(private songService: SongService) {
        this.songService = songService
    }

    router() {
        const router = express.Router();
        router.get('/', this.getAll.bind(this))
        router.get('/:id', this.getSongById.bind(this))

        router.post('/',this.addSong.bind(this));
        router.delete('/:id',this.deleteSong.bind(this));
        return router;
    }



    
    async getAll(req:express.Request, res: express.Response){
        const songs = await this.songService.getSong()
        res.json({isSuccess: 1, data: songs})
    }

    async getSongById(req:express.Request, res:express.Response){
        const song = await this.songService.getSongById(req.params.id)
        // console.log(song)
        res.json({isSuccess: 1, data: song})
    }

    async addSong(req:express.Request, res: express.Response){
        const newSong = {
            song_name: req.body.song_name,
            singer_id: req.body.singer_id,
            genre: req.body.genre
        }
        if(req.body.songname == '' || req.body.singername == '' || req.body.genre == ''){
            res.json({result:"invalid input"})
        }
        try {
            const result = await this.songService.addSong(newSong)
            if(result[0] > 0){
                res.json({result: 'add success'})

            }
        } catch(e) {
            res.status(500)
            res.json({result: 'add failed'})
            return
        }

    }

    
    async deleteSong(req:express.Request, res: express.Response){
        const result = this.songService.deleteSong(req.params.id);
        if(result) {
            res.json({delete: 1})
        } else {
            res.status(500)
            res.json({delete: 0})
        }
    }
}