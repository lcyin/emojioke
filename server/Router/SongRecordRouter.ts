import * as express from 'express';
import {SongRecordService} from '../Service/SongRecordService';

export class SongRecordRouter {
    constructor(private songRecordService: SongRecordService) {
    }

    router() {
        const router = express.Router();
        router.get('/', this.getAll.bind(this))
        router.post('/:id',this.getRecordById.bind(this));
        router.post('/',this.addSong.bind(this));
        router.delete('/:id',this.delete.bind(this));
        return router;
    }

    async getAll(req:express.Request, res: express.Response){
        const songRecord = await this.songRecordService.getSongRecord()
        res.json({ isSuccess:1,  data: songRecord})
    }


    async getRecordById(req:express.Request, res:express.Response){
        const record = await this.songRecordService.getRecordByIdSong(req.params.id)
        res.json({ isSuccess:1,  data: record})
    }

    async addSong(req:express.Request, res: express.Response){

        if(req.body.songname == "" || req.params.id == null || req.body.filename == ""  ){
            res.status(500)
            res.json({result: "empty input"})
            return
        } 

        try {
            const newSongRecord = {
                userId: req.params.id,
                songId: req.body.songId,
                filename: req.body.filename
            }
            const result = await this.songRecordService.addSongRecord(newSongRecord)
            if (result[0] > 0)
            res.json({result: 'add success'})
        } catch(e){
            res.status(500)
            res.json({result: 'add failed'})
            return
        }

    }

    
    async delete(req:express.Request, res: express.Response){

        const result = this.songRecordService.deleteSongRecord(req.params.id);
        result.then((data) => {
            if(data>0){
                res.json({result: 'delete 1'})
            } else {
                res.status(500)
                res.json({result: 'song records not exist'})
            }
        })

    }
}