import * as express from 'express';
import {UserService} from '../Service/UserService';
import * as multer from 'multer';



const storage = multer.diskStorage({
    
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/../assets/images/profile_pictures`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
const upload = multer({ storage })


export class UserRouter {
    // private userService:UserService
    constructor(private userService: UserService){
        // this.userService = userService
    }

    router() {
        const router = express.Router();
        router.get('/', this.levelGuard,this.get.bind(this))
        router.get('/:id',this.getByID.bind(this))
        router.post('/:id',upload.single('profile'),this.uploadProfile.bind(this));
        router.delete('/:id',this.levelGuard,this.delete.bind(this));
        return router;
    }

    
    levelGuard(req:express.Request, res: express.Response, next:express.NextFunction){
        if(typeof req.user !== 'undefined' && req.user.level === 'admin'){
            next()
        } else {
            res.status(401);
            res.json({result: 'not_authoized'});
        }
    }

    async get(req:express.Request, res: express.Response){

        const user = await this.userService.getUsers()
        res.json(user)
    }

    async getByID(req:express.Request, res: express.Response){

        if(req.user != undefined){
            const user = await this.userService.getUsersByID(req.params.id);
            res.json({isSuccess:1 ,data: user})

        } else {
            res.status(500)
            res.json({error: "user_not_found"})
        }
        
    }

    async delete(req:express.Request, res: express.Response){
        const result = this.userService.deleteUser(req.params.id);
        if(result) {
            res.json({delete: 1})
        } else {
            res.status(500)
            res.json({delete: 0})
        }
    }

    async uploadProfile(req:express.Request, res: express.Response){
        if(req.file){
            this.userService.uploadProfile(req.user.id,req.file.filename)
            res.json({result: 'upload success'})

        }
    }


    
}