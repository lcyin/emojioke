import { SongService } from '../Service/SongService'
import { SongRouter } from './SongRouter'

type Mockify<T> = {
    [P in keyof T]: jest.Mock<{}>;
  };

describe("Song Router", () => {
    let mockSongService:Mockify<SongService>;
    let songRouter: SongRouter;
    let req:any;
    let res:any;

    beforeEach(() => {
        mockSongService = {
            getSong: jest.fn(() => {}),
            addSong: jest.fn(() => {}),
            deleteSong: jest.fn(() => {})
        }

        req = {
            query: {},
            body: { },
            params: {},
        };
        res = {
            status: jest.fn(() => {}),
            json: jest.fn(() => {}),
        };
    
        songRouter = new SongRouter(mockSongService as any)
    })

    it("should get all song successfully", async () => {
        jest.spyOn(mockSongService, 'getSong').mockImplementation(() => {
            return {data:[{
                id: 1,
                songname: 'song1',
                genrn: 'rock',
                singername: 'john',
                filename: 'song.mp4'
            }]}
        })
        await songRouter.getAll(req, res);
        expect(res.json).toBeCalledWith({data:[{
            id: 1,
            songname: 'song1',
            genrn: 'rock',
            singername: 'john',
            filename: 'song.mp4'
        }]});
    })

    it("should add new song", async () => {
        jest.spyOn(mockSongService, 'addSong').mockReturnValue(true);

        req.body = {
            songname: "song2",
            singername: "John",
            genrn: "rap"

        }
        await songRouter.addSong(req,res);
        expect(res.json).toBeCalledWith({result: 1})
    })

    it("should failed to add user if the songname, singername, genrn is empty", async() => {
        req.body = {
            songname: "",
            singername: "",
            genrn: ""
        }
        await songRouter.addSong(req,res);
        expect(res.json).toBeCalledWith({result:'invalid input'})

    })
})