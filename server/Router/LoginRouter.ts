import * as express from 'express';
import { UserService } from '../Service/UserService';
import { SignupService } from '../Service/SignupService'
import * as jwtSimple from 'jwt-simple';
import {jwtConfig} from '../jwt';
import { checkPassword} from '../hash';
import fetch from 'cross-fetch'

export class LoginRouter{
    
    constructor(private userService:UserService, private signupService:SignupService){
    }
    router(){
        const router = express.Router();
        router.post('/',this.login);
        router.post('/facebook', this.loginFacebook)
        return router;
    }

    private login = async (req:express.Request,res:express.Response)=>{
        
        if (req.body.username && req.body.password) {
            const {username,password} = req.body;
            const user = (await this.userService.getUsers())
                    .find((user:any)=>
                        user.email == username &&
                        checkPassword(password,user.password)); 
            if (user) {
                const payload = {
                    id: user.id,
                    level: user.level
                };
                const token = jwtSimple.encode(payload, jwtConfig.jwtSecret);
                res.json({
                    token: token,
                    id: user.id
                });
            } else {
                res.sendStatus(401);
            }
        } else {
            res.sendStatus(401); 

        }
    }

    loginFacebook = async (req:express.Request,res:express.Response)=>{
        if (req.body.accessToken) {
            const {accessToken} = req.body;
            const fetchResponse =await fetch(`https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`);
            const result = await fetchResponse.json();
            if(result.error){
                return res.sendStatus(401);
            }
            let user = (await this.userService.getUsers())
                    .find((user:any)=>
                        user.email == result.email);
            // Create a new user if the user does not exist
            if (!user) {
                user = (await this.signupService.addUser(result.name,'',result.email, result.picture.data.url, 'user'))[0];
            }
            // console.log(user)
            const payload = {
                id: user.id,
                level: user.level

            };
            const token = jwtSimple.encode(payload, jwtConfig.jwtSecret);
            res.json({
                token: token,
                user: user.id
            });
            return res.send
        } else {
            return res.sendStatus(401); 
        }
    }

}