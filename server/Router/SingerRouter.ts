import * as express from 'express';

import {SingerService} from '../Service/SingerService';

export class SingerRouter {
    constructor(private singerService: SingerService){
    }

    router() {
        const router = express.Router();
        router.get('/', this.get.bind(this))
        router.post('/',this.post.bind(this));
        router.post('/:id', this.getSingerById.bind(this))
        router.delete('/:id',this.delete.bind(this));
        return router;
    }

    async get(req:express.Request, res: express.Response){
        const singer = await this.singerService.getSinger()
        res.json(singer)
    }

    async post(req:express.Request, res: express.Response){
        if(req.body.name == '' || req.body.gender == ''){
            res.status(500)
            res.json({result:'empty input'})
            return
        }
        try{
            const result = await this.singerService.addSinger(req.body.name, req.body.gender)

            if(result[0] > 0){
                res.json({result: 'success'})
            }

        } catch (e) {
                
            res.status(500)
            res.json({result: 'fail'})
            return
        }

    }

    async getSingerById(req:express.Request, res:express.Response){
        console.log(req.params.id)
        const singer = await this.singerService.getSingerById(req.params.id)
        res.json({isSuccess: 1, data: singer})
    }

    async delete(req:express.Request, res: express.Response){

        const result = this.singerService.deleteSinger(req.params.id);
        result.then((data)=> {
            if(data > 0){
                res.json({result: 'delete success'})
            } else {
                res.status(500)
                res.json({result: 'singer not exist'})
            }
        })

    }
}