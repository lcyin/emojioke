import * as Knex from "knex";

exports.seed = async function (knex: Knex) {
    await knex('users').del();
    await knex('singer').del();
    await knex('songs').del();
    await knex('song_records').del();
    await knex('emoji').del();
    await knex('song_records_emoji').del();
    await knex('comments').del();
    await knex('favourite_users').del();
    await knex('favourite_songs').del();

    await knex.insert(
        {
            username: "tecky",
            password: "$2b$10$/JVizhWL6srPSFLY/3SnEuun8NzjMgvhbBITZyEdvGbaQyBvRAOwO",
            email: "tecky.com",
            profile_pic: "profile-1548928552112.png",
            level: "admin"
        }).into("users");

    const userResult = await knex.insert(
        {
            username: "Dummy1",
            password: "$2b$10$eISt1nAub2nfVSFMW1I9PeE3FjSKcmOLnvPOx3bGUDdtdxlN16QYi",
            email: "Dummy1@dummy.com",
            profile_pic: "profile-1548928552112.png",
        }).into("users").returning("id");
    const returnedUserId = userResult[0];



    await knex.insert(
        {
            username: "Dummy2",
            password: "$2b$10$eISt1nAub2nfVSFMW1I9PeE3FjSKcmOLnvPOx3bGUDdtdxlN16QYi",
            email: "Dummy2@dummy.com",
            profile_pic: "profile-1548928457826.png",
        }).into("users");


    await knex.insert(
        {
            username: "Dummy3",
            password: "$2b$10$eISt1nAub2nfVSFMW1I9PeE3FjSKcmOLnvPOx3bGUDdtdxlN16QYi",
            email: "Dummy3@dummy.com",
            profile_pic: "profile-1548928552112.png",
        }).into("users");


    const singerBatch = [{
        gender: 'Female',
        name: 'Haim'
    },
    {
        gender: 'Male',
        name: 'Arctic Monkeys'
    },
    {
        gender: 'Male',
        name: 'Green Day'
    },
    {
        gender: 'Male',
        name: 'Imagine Dragons'
    },
    {
        gender: 'Male',
        name: 'Kings of Leon'
    }

    ]

    const returnedSingerIds = await knex.batchInsert('singer', singerBatch).returning('id').then((ids) => {
        return ids

    })

    const songBatch = [
        {
            singer_id: returnedSingerIds[0],
            song_name: "The Wire",
            genre: "Rock",
            filename: "Haim_TheWire",
            cover_image: "thewire"
        },
        {
            singer_id: returnedSingerIds[1],
            song_name: "Do I Wanna Know",
            genre: "Rock",
            filename: "ArcticMonkeys_DoIWannaKnow",
            cover_image: "am_doiwanttoknow"
        },
        {
            singer_id: returnedSingerIds[2],
            song_name: "Basket Case",
            genre: "Rock",
            filename: "GreenDay_BasketCase",
            cover_image: "basketcase"
        },
        {
            singer_id: returnedSingerIds[2],
            song_name: "Time Of Your Life",
            genre: "Rock",
            filename: "GreenDay_TimeOfYourLife",
            cover_image: "greenday_toyl"
        },
        {
            singer_id: returnedSingerIds[3],
            song_name: "Believer",
            genre: "Rock",
            filename: "ImagineDragons_Believer",
            cover_image: "ImaDrag_Blvr"
        },
        {
            singer_id: returnedSingerIds[4],
            song_name: "Sex on Fire",
            genre: "Rock",
            filename: "KingsofLeon_SexonFire",
            cover_image: "sexonfire"
        }

    ]


    const returnedSongIds = await knex.batchInsert('songs', songBatch).returning('id').then((ids) => {
        return ids
    });


    await knex.insert(
        {
            user_id: returnedUserId,
            song_id: returnedSongIds[0],
            filename: "ArcticMonkeys_DoIWannaKnow"
        }).into("song_records");

    const Song_RecordsResult = await knex.insert(
        {
            user_id: returnedUserId,
            song_id: returnedSongIds[0],
            filename: "GreenDay_BasketCase"
        }).into("song_records").returning("id");
    const returnedSong_RecordsId = Song_RecordsResult[0];

    await knex.insert(
        {
            type: 'very_bad',
            value: 0,
            filename: "aFileName"
        }).into("emoji")

    await knex.insert(
        {
            type: 'bad',
            value: 0.25,
            filename: "aFileName"
        }).into("emoji");
    await knex.insert(
        {
            type: 'neutral',
            value: 0.5,
            filename: "aFileName"
        }).into("emoji");
    await knex.insert(
        {
            type: 'good',
            value: 0.75,
            filename: "aFileName"
        }).into("emoji");
    const emojiResult = await knex.insert(
        {
            type: 'very_good',
            value: 1,
            filename: "aFileName"
        }).into("emoji").returning("id");
    const returnedEmojiId = emojiResult[0];

    await knex.insert([
        {
            user_id: returnedUserId,
            emoji_id: returnedEmojiId,
            song_record_id: returnedSong_RecordsId,
            clicked_time: 0.25,
            clicked_coord_x: 0.45,
            clicked_coord_y: 0.76
        },
        {
            user_id: returnedUserId,
            emoji_id: returnedEmojiId,
            song_record_id: returnedSong_RecordsId,
            clicked_time: 5,
            clicked_coord_x: 0.45,
            clicked_coord_y: 0.76
        }, {
            user_id: returnedUserId,
            emoji_id: returnedEmojiId,
            song_record_id: returnedSong_RecordsId,
            clicked_time: 8,
            clicked_coord_x: 0.45,
            clicked_coord_y: 0.76
        }]
    ).into("song_records_emoji");

    await knex.insert(
        {
            user_id: returnedUserId,
            song_record_id: returnedSong_RecordsId,
            comments: "a comment",
            time: 1.45
        }).into("comments");

    await knex.insert(
        {
            user_id: returnedUserId,
            followed_user_id: returnedUserId,
        }).into("favourite_users");

    await knex.insert(
        {
            user_id: returnedUserId,
            song_record_id: returnedSong_RecordsId,
        }).into("favourite_songs");
};