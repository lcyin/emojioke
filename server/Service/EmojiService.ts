import * as Knex from 'knex';
// import { Emoji } from '../interface';


export class EmojiService {

    constructor(private knex: Knex) {

    }

    async getEmoji() {
        const emoji = await this.knex.select("*").from('emoji')
        return emoji
    }

    async getEmojiById(id:number){
        const emoji = await this.knex.select("*").from("emoji").where("id", id)
        return emoji
    }


    async addEmoji(body: any) {
        const value = Number(body.value)
        const newEmoji = {
            'type': body.type,
            'value': value,
            'filename': body.filename
        }
        return this.knex.insert(newEmoji).into("emoji").returning("id")
    }
}