import * as Knex from 'knex';


export class SingerService {

    constructor(private knex: Knex) {

    }

    async getSinger() {
        const singer = await this.knex.select("id", "name", "gender").from('singer')
        return singer
    }

    async getSingerById(id:number){
        console.log(id)
        const singer = await this.knex.select("*").from("singer").where("id", id)
        return singer
    }

    async addSinger(name: string, gender: string) {

            const newSinger = {
                'name': name,
                'gender': gender,
            }

            return this.knex.insert(newSinger).into("singer").returning("id")
    }


    async deleteSinger(id:number) {
        return this.knex('singer')
        .where('id', id)
        .del()

    }
}