const util = require('util');
const exec = util.promisify(require('child_process').exec);


export class UploadService{
    constructor() {

    }


    async convertVideo(file:any){

        const uniqueAudioName = Date.now();
        const uniqueVideoName = Date.now();
        const uniqueFinalName = Date.now();

        await exec(`ffmpeg -i ${file.path} -i "./assets/OGSongs/extractedAudio/${file.originalname}.mp3" -filter_complex amerge -c:a libmp3lame -q:a 4 "./uploads/combinedAudio/${uniqueAudioName}.mp3"`, {
            env: {
                PATH: process.env.PATH
            }
        });
        await exec(`ffmpeg -i "./assets/OGSongs/${file.originalname}.mp4" -an "./uploads/strippedVideo/${uniqueVideoName}.mp4"`, {
            env: {
                PATH: process.env.PATH
            }
        });
        await exec(`ffmpeg -i "./uploads/strippedVideo/${uniqueVideoName}.mp4" -i "./uploads/combinedAudio/${uniqueAudioName}.mp3" -shortest -strict -2 "./uploads/finalEncodedFile/${uniqueFinalName}.mp4"`, {
            env: {
                PATH: process.env.PATH
            }
        });

        return ({isSuccess: 1, filename: uniqueFinalName})
    }
    
}