import { UserService } from './UserService'
import { User } from '../interface';

describe("UserService", () => {
    let userService:UserService;
    let initialUser:Array<User>;


    beforeEach(() => {
        initialUser = [{
            username: 'tecky',
            password: 'tecky',
            email: 'tecky.mail'
        }]
        userService = new UserService(initialUser);
    })

    it("should return initialUsers", ()=> {
        const res = userService.getUsers()
        expect(res).toEqual([{
            username: 'tecky',
            password: 'tecky',
            email: 'tecky.mail'
        }])
    })

    it("should addUser successfully", () => {
        const newUser:User={
            username: 'kevin',
            password: 'kevin',
            email: 'kevin'
        }

        const res = userService.addUser(newUser)
        expect(res).toEqual({"result": true})
    })

    it("should addUser failed" ,() => {
        const newUser={
            id: 3,
            username: 'kevin',
            password: 'kevin',
            email: null
        }

        const res = userService.addUser(newUser)
        expect(res.result).toEqual(false)
    })

    it("should delete user successfully", () => {

        const newUser:User={
            username: 'kevin',
            password: 'kevin',
            email: 'kevin'
        }

        userService.addUser(newUser)
        const res1 = userService.deleteUser(1)
        
        expect(res1).toEqual({result: true})
    })

    it('should delete user failed', () => {
        const newUser={
            id: 2,
            username: 'kevin',
            password: 'kevin',
            email: 'kevin'
        }

        userService.addUser(newUser)
        const res1 = userService.deleteUser(3)
        expect(res1).toEqual({result: false})

    })

    it('should update user successfully', () => {
        const updateUser = {
            username: 'john',
            password: 'john',
            email: 'johm.mail'
        }

        const res = userService.updateUser(1,updateUser)
        expect(res).toEqual(1)
    })

    it('should update user failed', () => {
        const updateUser = {
            username: 'john',
            password: 'john',
            email: 'johm.mail'
        }

        const res = userService.updateUser(2,updateUser)
        expect(res).toEqual(0)
    })
})