import * as Knex from 'knex';
// import { Song } from '../interface';


export class SongService {

    constructor(private knex: Knex) {

    }

    async getSong() {
        const songs = await this.knex('songs')
        .join('singer', 'songs.singer_id', 'singer.id')
        .select('songs.id',
                'songs.song_name',
                'songs.genre',
                'songs.filename',
                'songs.cover_image',
                'singer.name',
                'singer.gender')
        return songs

    }

    async getSongById(id:number){
        // const song = await this.knex.select("*").from("songs").where("id", id)

        const song = await this.knex('songs')
        .join('song_records', 'songs.id', 'song_records.song_id')
        .where('song_records.id', id)
        .select("*")
        return song
    }


    async addSong(body: any) {


        const newSong = {
            'singer_id': body.singer_id,
            'song_name': body.song_name,
            'genre': body.genre
        }
        return this.knex.insert(newSong).into("songs").returning("id")
    }

    async deleteSong(id: number) {
        if (id != undefined) {
            return this.knex('songs')
                .where('id', id)
                .del()
        } else {
            return { result: 0 };
        }
    }
}
