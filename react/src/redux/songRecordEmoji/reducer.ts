import { IEmojiRecordListState } from './state'
import { IGetEmojiRecordActions , IAddEmojiRecordActions } from './actions'

const initialState: IEmojiRecordListState = {
  emojiRecords: {
    emojiList:[],
    data:[]
  },
  error: 'null'
}

export function viewSongEmojiRecord(state: IEmojiRecordListState = initialState, action: IGetEmojiRecordActions) {
  switch (action.type) {
    case "GET_EMOJI_RECORD_SUCCESS":
      // alert(JSON.stringify(action.emojiRecords))
      const emojiRecords = action.emojiRecords.data.slice();
      const emojiList = action.emojiRecords.emojiList.slice();
      const emojiRecordsState = {
        emojiList,
        data:emojiRecords
     }
      return {
        emojiRecords: emojiRecordsState,
        error: state.error
      };
    case "GET_EMOJI_RECORD_FAILED":
      return Object.assign({}, state, action.message);
      ;
    default:
      return state;
  }
}

export function addSongEmojiRecord(state: IEmojiRecordListState = initialState, action: IAddEmojiRecordActions) {
  switch (action.type) {
    case "ADD_EMOJI_RECORD_SUCCESS":
      alert("ADD_EMOJI_RECORD_SUCCESS !!!!!!!")
      return state;
    case "ADD_EMOJI_RECORD_FAILED":
      return Object.assign({}, state, action.message);
    default:
      return state;
  }
}