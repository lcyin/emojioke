import { ThunkResult } from 'src/Store';
import { Dispatch } from 'redux';
import { IUserRecordState } from './state';

type GET_USER_SUCCESS = 'GET_USER_SUCCESS';
type GET_USER_FAILED = 'GET_USER_FAILED';

type UPLOAD_SUCCESS = 'UPLOAD_PROFILE_SUCCESS';
type UPLOAD_FAILED = 'UPLOAD_PROFILE_FAILED';

const {REACT_APP_API_SERVER} = process.env;

export function getUser(id:any):ThunkResult<void>{
    return async (dispatch:Dispatch<IGetUserActions>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/users/${id}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        });
        
        const result = await res.json()
        if(result.isSuccess) {
            dispatch(getUserSucccess(result.data as any))
        } else {
            // dispatch(getSongSucccess(result as any))
            dispatch(getUserFailed(result))
        }
    }
}

export function uploadProfile(id:number,file:File):ThunkResult<void>{
    return async (dispatch:Dispatch<IGetUserActions>) => {
        const formData = new FormData();
        formData.append('id',id.toString());
        formData.append('profile',file);
        const res = await fetch(`${REACT_APP_API_SERVER}/users/${id}`, {
            method: 'POST',
            headers:{
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body: formData
        });
        
        const result = await res.json()
        if(result.isSuccess) {
            dispatch(uploadProfileSuccess())
        } else {
            // dispatch(getSongSucccess(result as any))
            dispatch(uploadProgilefailed(result.msg))
        }
    }
}

export function getUserSucccess(user: any):IGetUserSuccessAction {
    return {
        type: 'GET_USER_SUCCESS',
        user
    }
}

export function uploadProfileSuccess():IUploadProfileSuccessAction {
    return {
        type: 'UPLOAD_PROFILE_SUCCESS',
    }
}

export function getUserFailed(message: string):IGetUserFailedAction {
    return {
        type: 'GET_USER_FAILED',
        message
    }
}

export function uploadProgilefailed(message: string):IUploadProfileFailedAction {
    return {
        type: 'UPLOAD_PROFILE_FAILED',
        message
    }
}

interface IGetUserSuccessAction {
    type: GET_USER_SUCCESS,
    user: IUserRecordState
};

interface IUploadProfileSuccessAction {
    type: UPLOAD_SUCCESS,
};


interface IGetUserFailedAction {
    type: GET_USER_FAILED,
    message: string
};

interface IUploadProfileFailedAction {
    type: UPLOAD_FAILED,
    message: string
};

export type IGetUserActions = IGetUserSuccessAction| IGetUserFailedAction | IUploadProfileSuccessAction |IUploadProfileFailedAction


// type GET_SONG_RECORD_SUCCESS = 'GET_SONG_RECORD_SUCCESS';
// type GET_SONG_RECORD_FAILED = 'GET_SONG_RECORD_FAILED';