import { ISongRecordListState } from './state'
import { IGetSongRecordActions } from './actions'

const initialState: ISongRecordListState = {
  songRecord: {
    user_id: 1,
    song_id: 1,
    filename: "1",
    created_at: ''
  },
  error: 'null'
}

export function viewSongRecording(state: ISongRecordListState = initialState, action: IGetSongRecordActions) {
  switch (action.type) {
    case "GET_SONG_RECORD_SUCCESS":
      // alert(JSON.stringify(action))
      const newSongState = action.songRecord
      console.log(newSongState)
      return {
        songRecord: newSongState,
        error: state.error
      };
    case "GET_ALL_SONG_RECORD_SUCCESS":
      // alert(JSON.stringify(action))
      const newRecordListState = action.songRecordList
      return {
        songRecord: newRecordListState,
        error: state.error
      };
    case "GET_SONG_RECORD_FAILED":
      return Object.assign({}, state, action.message);
      ;
    case "GET_ALL_SONG_RECORD_FAILED":
      return Object.assign({}, state, action.message);
      ;
    default:
      return state;
  }
}