import { ThunkResult } from 'src/Store';
import { Dispatch } from 'redux';
import { ISongRecordState } from './state';

type GET_SONG_RECORD_SUCCESS = 'GET_SONG_RECORD_SUCCESS';
type GET_SONG_RECORD_FAILED = 'GET_SONG_RECORD_FAILED';
type GET_All_SONG_RECORD_SUCCESS = 'GET_ALL_SONG_RECORD_SUCCESS';
type GET_All_SONG_RECORD_FAILED = 'GET_ALL_SONG_RECORD_FAILED';


const {REACT_APP_API_SERVER} = process.env;

export function viewSongRecord(id:any):ThunkResult<void>{
    return async (dispatch:Dispatch<IGetSongRecordActions>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/songRecord/${id}`, {
            method: 'POST',
            headers: {
                // 'Authorization': `Bearer ${localStorage.getItem('token')}`
                // 'Content-Type': 'application/json'
            }
        });


        const result = await res.json()
        console.log(result)
        // alert(JSON.stringify(result));
        if(result.isSuccess) {
            dispatch(getSongRecordSucccess(result.data as any))
        } else {
            // dispatch(getSongSucccess(result as any))
            dispatch(getSongRecordFailed(result))
        }
    }
}


export function getSongRecord():ThunkResult<void>{
    return async (dispatch:Dispatch<IGetSongRecordActions>) => {


        const res = await fetch(`${REACT_APP_API_SERVER}/songRecord`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
                // 'Content-Type': 'application/json'
            }
        });
        
        const result = await res.json()

        console.log(result)
        // alert(JSON.stringify(result));
        if(result.isSuccess) {
            dispatch(getAllSongRecordSucccess(result.data as any))
        } else {
            // dispatch(getSongSucccess(result as any))
            dispatch(getAllSongRecordFailed(result))
        }
    }
}



export function getSongRecordSucccess(songRecord: any):IGetSongRecordSuccessAction {
    return {
        type: 'GET_SONG_RECORD_SUCCESS',
        songRecord
    }
}


export function getAllSongRecordSucccess(songRecordList: any):IGetAllSongRecordSuccessAction {
    return {
        type: 'GET_ALL_SONG_RECORD_SUCCESS',
        songRecordList
    }
}


export function getSongRecordFailed(message: string):IGetSongRecordFailedAction {
    return {
        type: 'GET_SONG_RECORD_FAILED',
        message
    }
}

export function getAllSongRecordFailed(message: string):IGetAllSongRecordFailedAction {
    return {
        type: 'GET_ALL_SONG_RECORD_FAILED',
        message
    }
}

interface IGetSongRecordSuccessAction {
    type: GET_SONG_RECORD_SUCCESS,
    songRecord: ISongRecordState
};

interface IGetAllSongRecordSuccessAction {
    type: GET_All_SONG_RECORD_SUCCESS,
    songRecordList: any
};

interface IGetSongRecordFailedAction {
    type: GET_SONG_RECORD_FAILED,
    message: string
};

interface IGetAllSongRecordFailedAction {
    type: GET_All_SONG_RECORD_FAILED,
    message: string
};

export type IGetSongRecordActions = IGetSongRecordSuccessAction| IGetSongRecordFailedAction | IGetAllSongRecordSuccessAction | IGetAllSongRecordFailedAction