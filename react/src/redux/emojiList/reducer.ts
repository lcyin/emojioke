import { IEmojiListState } from './state'
import { IEmojiListActions } from './actions'

const initialState: IEmojiListState = {
  emojiList: [],
  error: 'null'
}

export function viewEmojiList(state: IEmojiListState = initialState, action: IEmojiListActions) {
  switch (action.type) {
    case "GET_EMOJI_LIST_SUCCESS":
      // alert(JSON.stringify(action))
      const newEmojiState = action.emojiList.slice()
      return {
        emojiList: newEmojiState,
        error: state.error
      };
    case "GET_EMOJI_LIST_FAILED":
      return Object.assign({}, state, action.message);
      ;
    default:
      return state;
  }
}