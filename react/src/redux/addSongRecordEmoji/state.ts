
export interface IEmojiRecordState {
   emojiList: IEmojiState[],
   data:IEmojiRecordDataState[]
}

export interface IEmojiState {
   id:number  
   type:string   
   value:number 
   filename:string
   emojiCount:number
}

export interface IEmojiRecordDataState {
   id:number
   user_id: string
   emoji_id: number 
   time : string
   song_record_id : number
   clicked_time:number
   clicked_coord_x:number
   clicked_coord_y:number
}


export interface IEmojiRecordListState {
   emojiRecords: IEmojiRecordState,
   error: string ;
}