import { IEmojiRecordListState } from './state'
import { IAddEmojiRecordActions } from './actions'

const initialState: IEmojiRecordListState = {
  emojiRecords: {
    emojiList:[],
    data:[]
  },
  error: 'null'
}

export function addSongEmojiRecord(state: IEmojiRecordListState = initialState, action: IAddEmojiRecordActions) {
  switch (action.type) {
    case "ADD_EMOJI_RECORD_SUCCESS":
      alert("ADD_EMOJI_RECORD_SUCCESS !!!!!!!")
      return state;
    case "ADD_EMOJI_RECORD_FAILED":
      return Object.assign({}, state, action.message);
    default:
      return state;
  }
}