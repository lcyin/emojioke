import { ISongListState } from './state'
import { ISongSelectActions } from './action'

const initialState:ISongListState = {
    songList: [{
        id: '',
        song_name: '',
        genre: '',
        filename: '',
        cover_image: '',
        name: '',
        singer_gender: ''

    }],
    error: 'null'
}

export function songReducer(state: ISongListState = initialState, action: ISongSelectActions) {
    switch (action.type) {
        case "GET_SONG_SUCCESS":
        const newSongState = action.songList.slice()
            return {
                songList: newSongState,
                error: state.error
            };
        case "GET_SONG_FAILED":
        return Object.assign({}, state, action.message );
        ;
        default:
            return state;
    }
}