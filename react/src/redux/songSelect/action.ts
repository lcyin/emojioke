import { ThunkResult } from 'src/Store';
import { Dispatch } from 'redux';
import { ISongState } from './state';

type GET_SONG_SUCCESS = 'GET_SONG_SUCCESS';
type GET_SONG_FAILED = 'GET_SONG_FAILED';

const { REACT_APP_API_SERVER } = process.env;

export function getSong(): ThunkResult<void> {
    return async (dispatch: Dispatch<ISongSelectActions>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/song`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });

        const result = await res.json()

        if (result.isSuccess) {
            dispatch(getSongSuccess(result.data as any))
        } else {
            dispatch(getSongFailed(result))
        }
    }
}


export function getSongSuccess(songs: ISongState[]): IGetSongSuccessAction {
    return {
        type: 'GET_SONG_SUCCESS',
        songList: songs
    }
}

export function getSongFailed(message: string): IGetSongFailedAction {
    return {
        type: 'GET_SONG_FAILED',
        message
    }
}

interface IGetSongSuccessAction {
    type: GET_SONG_SUCCESS,
    songList: ISongState[]
};

interface IGetSongFailedAction {
    type: GET_SONG_FAILED,
    message: string
};

export type ISongSelectActions = IGetSongSuccessAction | IGetSongFailedAction