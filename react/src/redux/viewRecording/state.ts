export interface ISongState2 {
   singer_id: string
   song_name: string 
   genre: string 
}

export interface ISelectedSongListState {
   selectedSong: ISongState2[]
   error: string ;
}