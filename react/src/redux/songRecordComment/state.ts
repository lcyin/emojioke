export interface ICommentState {
   user_id: string
   comments: string 
   time : string
}

export interface ICommentListState {
   comments: ICommentState[]
   error: string ;
}