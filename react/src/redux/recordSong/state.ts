export interface IrecordSong {
   fluid: boolean,
   controls: boolean,
   sources: Array<{
      src: string,
      type: string
   }>
}

export interface IImportSongState {
   songImport: IrecordSong[]
   error: string;
}