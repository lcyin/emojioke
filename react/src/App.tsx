import * as React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Navbar from './components/navBar/Navbar';
import SignUp from './components/signup/SignUp';
import Login from './components/login/Login';
import ViewRecording from './components/video/View-Recording';
import NotFound from './NotFound';
import Profile from './components/profile/Profile';
import SongSelect from './components/songSelect/SongSelect';
import RecordSong from './components/recordSong/RecordSong';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import store, {history} from './Store';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Home from './components/home/Home';
import 'src/assets/styles/global.css';
import { PrivateRoute } from './PrivateRoute';

import Footer from './components/footer/footer';

class App extends React.Component {

  public render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <div>
            <Navbar />
            <Switch>
              <Route path="/" exact={true} component={Home} />
              <Route path="/login" component={Login} />
              <Route path="/signup" component={SignUp} />
              <Route path="/video/" exact={true}><Redirect to="/" /></Route>
              <Route path="/video/:id" component={ViewRecording} />
              <PrivateRoute path="/profile" component={Profile} />
              <PrivateRoute path="/song_select" component={SongSelect} />
              <PrivateRoute path="/record_song/:filename/:id" component={RecordSong} />
              <Route component={NotFound} />
            </Switch>
            <Footer />
          </div>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
