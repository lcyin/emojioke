import * as React from 'react';
import './SignUp.css';
import { connect } from 'react-redux';
import { signup } from 'src/redux/signup/action';
import { IRootState, ThunkDispatch } from 'src/Store';
import signUpImg from '../../assets/images/signup.png';
import { Button } from 'reactstrap';

// import drawingMic from '../../assets/images/drawing_mic.png';
// import drawingEmoji from '../../assets/images/drawing_emoji2.png';

interface ISignupState {
  username: string,
  password: string,
  email: string
}

interface ISignupProps {
  signup: (username: string, password: string, email: string) => void
}

class SignUp extends React.Component<ISignupProps, ISignupState> {
  constructor(props: ISignupProps) {
    super(props)
    this.state = {
      username: "",
      password: "",
      email: ""
    }
  }

  private onChange = (field: 'username' | 'password' | 'email', e: React.FormEvent<HTMLInputElement>) => {
    const state = {};
    state[field] = e.currentTarget.value;
    this.setState(state);
  }

  private onSubmit = (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    this.props.signup(this.state.username, this.state.password, this.state.email)
  }

  public render() {
    return (
      <div id="signUpBackgroundImage">
        <form>
          <div className="center">
            <h4 style={{ "fontFamily": "Shadows Into Light Two Regular" }}>Sign-Up</h4>
          </div>
          <div className="imgcontainer">
            <img src={signUpImg} alt="" className="signUpImg" />
          </div>

          <div className="signUpContainer">

            <label>
              <b>Email</b>
            </label>
            <input type="text"
              placeholder="Enter Email"
              value={this.state.email}
              onChange={this.onChange.bind(this, 'email')}
              required />

            <label>
              <b>Password</b>
            </label>
            <input type="password"
              placeholder="Enter Password"
              value={this.state.password}
              onChange={this.onChange.bind(this, 'password')}
              required />

            <label>
              <b>Display Name</b>
            </label>
            <input type="text"
              placeholder="Enter Username"
              value={this.state.username}
              onChange={this.onChange.bind(this, 'username')}
              required />

            <Button color="info" type="submit" onClick={this.onSubmit}>Create Account</Button>
          </div>

        </form>
      </div>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({});


const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  signup: (username: string, password: string, email: string) => {
    dispatch(signup(username, password, email))
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)
