// emoji icon image
import very_good from 'src/assets/images/emoji/very_good.png';
import good from 'src/assets/images/emoji/good.png';
import neutral from 'src/assets/images/emoji/neutral.png';
import bad from 'src/assets/images/emoji/bad.png';
import very_bad from 'src/assets/images/emoji/very_bad.png';
// default emoji icon image
import defaultIcon from 'src/assets/images/microphoneIcon.png';

export default function returnIcon (emojiID: number) {
   switch (emojiID) {
      case 5:
         return very_good
         break;
      case 4:
         return good
         break;
      case 3:
         return neutral
         break;
      case 2:
         return bad
         break;
      case 1:
         return very_bad
         break;
      default:
         return defaultIcon
   }
}