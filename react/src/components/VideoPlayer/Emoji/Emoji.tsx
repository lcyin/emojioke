import * as React from 'react';
import './Emoji.css'

// import { connect } from 'react-redux';

// import { ThunkDispatch, IRootState } from 'src/Store';

// import { viewSongEmojiRecord } from 'src/redux/songRecordEmoji/actions';

// const { Player } = require('video-react');
import "node_modules/video-react/dist/video-react.css";

class Emoji extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    
  }

  public render() {
    const divStyle: any = {
      position: 'absolute',
      top: this.props.Emoji.clicked_coord_y+"%",
      left: this.props.Emoji.clicked_coord_x+"%",
      // paddingLeft:this.props.Emoji.clicked_coord_x+"%",
      zIndex: '2',
      width: '40px',
      height: '40px'
    };
    const imgStyle: any = {
      maxWidth: "100%",
      maxHeight: "100%"
    };
    return (
      <div className="videoEmoji" style={divStyle} ><img src={this.props.src} style={imgStyle} /></div>
    );
  }
}

export default Emoji;







