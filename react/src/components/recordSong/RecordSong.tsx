import * as React from 'react';
import './RecordSong.css';
import './video-react.css';
import { connect } from 'react-redux';
import { IRootState } from '../../Store';
const { Player } = require('video-react');
import { Dispatch } from 'redux';
import { importSong } from 'src/redux/recordSong/action';
import { IrecordSong } from 'src/redux/recordSong/state';
import { match } from 'react-router';
const Recorder = require('./react-mp3-recorder').default;
import { Tooltip, Modal, ModalHeader, ModalBody, Spinner,
  //  ModalFooter, Button
   } from 'reactstrap';
import { push } from 'connected-react-router';


interface IPropsRecordSong {
  video: IrecordSong[],
  importSong: (filename: string) => {},
  goHome: () => {},
  back: () => {}
}

interface IStateRecordSong {
  tooltipOpen: boolean,
  isRecording: boolean | null,
  modal: boolean,

}

export interface IOwnProps {
  match: match<{ filename: string, id: string }>,
  handleSubmit: string,
  id: string,
}

class RecordSong extends React.Component<IOwnProps & IPropsRecordSong, IStateRecordSong> {
  private recorder: any;
  private player: any
  constructor(props: IOwnProps & IPropsRecordSong) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      tooltipOpen: false,
      isRecording: false,
      modal: false
    }
    this.player = React.createRef()
    this.recorder = React.createRef()

  }


  public toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

  public componentDidMount() {
    this.props.importSong(this.props.match.params.filename);
    this.player.current.subscribeToStateChange(this.handleStateChange.bind(this));
    console.log(this.recorder.current)
  }
  public componentDidUpdate() {

    if (this.state.isRecording) {
      this.player.current.play()
    } else {
      this.player.current.pause()
    }

  }

  private renderModal() {
    return <Modal isOpen={this.state.modal}  >
      <ModalHeader>
        Uploading
        </ModalHeader>
      <ModalBody>
        <Spinner color="dark" />
      </ModalBody>
    </Modal>
  }


  private handleStateChange(state: any, prevState: any) {
    console.log()
  }

  private onRecordingStart = () => {
    this.setState({
      isRecording: true
    })
  }

  private onRecordingComplete = async (blob: any) => {
    this.setState({
      isRecording: false,
      modal: true
    })
    const file = new File([blob], this.props.match.params.filename, { lastModified: Date.now(), type: "audio/mp3" });
    const formData = new FormData();
    formData.set('song_id', this.props.match.params.id);
    formData.append('mp3', file)
    const res = await fetch(`${process.env.REACT_APP_API_SERVER}/uploads`, {
      method: 'POST',
      body: formData,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    const result = await res.json()
    if (result.isSuccess) {
      this.setState({
        modal: false
      });
      this.props.goHome()
    }
  }

  private onRecordingError = (err: any) => {
    // console.log('recording error', err)
  }

  public render() {
    return (

      // <Modal isOpen={true} size="lg" dialogClassName="my-modal" >
      //   <ModalHeader>
      //     <h3 style={{ "fontFamily": "Shadows Into Light Two Regular" }} className="pageTitle text-white">Sing and Record Song Here</h3>
      //   </ModalHeader>
      //   <ModalBody  id="recordSongBackgroundImage">
      //     <Player className="reactPlayer"
      //       ref={this.player}
      //     >
      //       <source src={this.props.video[0].sources[0].src} />
      //     </Player>

      //   </ModalBody>
      //   <ModalFooter>
      //     <Recorder id="Tooltip"
      //       onRecordingComplete={this.onRecordingComplete}
      //       onRecordingError={this.onRecordingError}
      //       onRecordingStart={this.onRecordingStart}
      //       ref={this.recorder}
      //     />
      //     <Tooltip placement="right" isOpen={this.state.tooltipOpen} target="Tooltip" toggle={this.toggle}>
      //       Click to start song and recording
      //       </Tooltip>
      //   </ModalFooter>
      //   <Button onClick={this.props.back}>Back</Button>
      //   { this.state.modal ? this.renderModal() : "" }

      // </Modal>
      <div id="recordSongBackgroundImage">
        <div className="container">
          <h3 style={{ "fontFamily": "Shadows Into Light Two Regular" }} className="pageTitle text-white">Sing and Record Song Here</h3>
          <div className="row rowClass">
            <div>
              <Player className="reactPlayer"
                ref={this.player}
              >
                <source src={this.props.video[0].sources[0].src} />
              </Player>
              <Recorder id="Tooltip"
                onRecordingComplete={this.onRecordingComplete}
                onRecordingError={this.onRecordingError}
                onRecordingStart={this.onRecordingStart}
                ref={this.recorder}
              />
        <Tooltip placement="right" isOpen={this.state.tooltipOpen} target="Tooltip" toggle={this.toggle}>
          Click to start song and recording
      </Tooltip>
            </div>
          </div>
        </div>
        { this.state.modal ? this.renderModal() : "" }

      </div>
    );

  }
}

const mapStateToProps = (state: IRootState) => ({
  video: state.importSong.songImport
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  importSong: (filename: string) => {
    dispatch(importSong(filename))
  },
  goHome: () => {
    dispatch(push('/'))
  },
  back: () => {
    dispatch(push('/song_select'))

  }
})

export default connect(mapStateToProps, mapDispatchToProps)(RecordSong);