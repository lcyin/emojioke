import * as React from 'react';
class Expire extends React.Component<any, any>{
   private timers: any
   constructor(props: any) {
      super(props);
      this.state = { visible: true }
   }

   public componentWillReceiveProps(nextProps: any) {
      // reset the timer if children are changed
      if (nextProps.children !== this.props.children) {
         this.setTimer();
         this.setState({ visible: true });
      }
   }

   public componentDidMount() {
      this.setTimer();
   }

   public setTimeoutSelf = () => {
      this.setState({ visible: false });
      this.timers = null;
   }

   private setTimer() {
      // clear any existing timer
      if (this.timers != null) {
         clearTimeout(this.timers)
      }

      // hide after `delay` milliseconds
      this.timers = setTimeout(
         this.setTimeoutSelf, this.props.delay);
   }

   public componentWillUnmount() {
      clearTimeout(this.timers);
   }

   public render() {
      return this.state.visible
         ? <div>{this.props.children}</div>
         : '';
   }
}

export default Expire;