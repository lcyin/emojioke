import * as React from 'react';
import './VideoItem.css';

interface IVideoItemProps {
   user_id: number,
   song_id: number,
   filename: string
}


function VideoItem(props: IVideoItemProps) {
   return (
      <div>
         <div>
            User ID : {props.user_id}
         </div>
         <div>
            Song ID : {props.song_id}
         </div>
         <div>
            filename : {props.filename}
         </div>
      </div>
   );
}


export default VideoItem;