import * as React from 'react';
import './View-Recording.css'
import { match, Redirect } from 'react-router-dom';
import { ThunkDispatch, IRootState } from 'src/Store';

// import
import EmojiBar from './emojiBar/EmojiBar';
import SongDetail from './songDetailDiv/SongDetail';
import VideoPlayer from '../VideoPlayer/Video-Player';

import { connect } from 'react-redux'
import { getEmojiList } from 'src/redux/emojiList/actions';
import { viewSongRecord } from 'src/redux/songRecord/actions';

import Emoji from 'src/components/VideoPlayer/Emoji/Emoji';
import returnIcon from 'src/components/Emoji'
import { Container, Row, Col } from 'reactstrap';

interface IBoardProps {
  match: match<{ id: any }>,
  emojiList: any,
  getEmojiList: any,
  viewSongRecord: any,
  songRecord: any

}
class ViewRecording extends React.Component<IBoardProps, any> {
  public videoPlayer: any;
  public videoTime: any = 2;
  public isLogin = false;
  public emojiShowSecond = 3;
  public maxCountOfShowingEmoji = 20;

  constructor(props: any) {
    super(props);
    this.state = {
      filename: '',
      displayUserAddedEmoji: []
    }
  }

  public componentDidMount() {
    this.props.getEmojiList();
    this.props.viewSongRecord(this.videoID());
  }

  private videoID() {
    const id = parseInt(this.props.match.params.id, 10);
    return id;
  }

  private userID() {
    const userid = localStorage.getItem('user');
    if (userid) {
      this.isLogin = true;
      return userid;
    }
    return '';
  }

  public setVideoTime = (videoTime: any) => {
    this.videoTime = videoTime;
  }

  public getVideoTime = () => {
    return this.videoTime
  }
  public randomPosition(max: number) {
    return Math.floor(Math.random() * Math.floor(max))
  }
  public pushDisplayUserAddedEmoji = (id: any) => {
    const newEmojiData = { id, clicked_coord_y: this.randomPosition(90), clicked_coord_x: this.randomPosition(90), src: returnIcon(id) };
    const newState = this.state.displayUserAddedEmoji.slice();
    if (this.state.displayUserAddedEmoji.length < this.maxCountOfShowingEmoji) {
      newState.push({ data: newEmojiData });
      setTimeout(this.removeItemFromDisplayUserAddedEmojiArray, this.emojiShowSecond * 1000);
    }
    this.setState({ displayUserAddedEmoji: newState });
  }

  public setDisplayUserAddedEmoji = (newState: any) => {
    this.setState({ displayUserAddedEmoji: newState });
  }

  public removeItemFromDisplayUserAddedEmojiArray = () => {
    const newState = this.state.displayUserAddedEmoji;
    newState.shift();
    this.setState({ displayUserAddedEmoji: newState });
  }

  public getDisplayUserAddedEmoji = () => {
    return this.state.displayUserAddedEmoji
  }

  public getVideoPlayer = () => {
    if (this.props.songRecord[0] !== undefined) {
      const filename = this.props.songRecord[0].filename;
      return <Row>
        <Col  sm="8" md={{ size: 6, offset: 3 }}>
          <VideoPlayer userAddedEmoji={this.getDisplayUserAddedEmoji()} setCurrent={this.setVideoTime} src={`${process.env.REACT_APP_API_SERVER}/finalEncodedFile/${filename}.mp4`} />
        </Col>

      </Row>
    }
    return '';
  }
  public getSongDetail = () => {
    if (this.props.songRecord[0] !== undefined) {
      const songRecordsId = this.props.songRecord[0].id;

      return <div className="row emojiDetail">< SongDetail videoId={songRecordsId} /></div>
    }
    return '';
  }
  public getEmojiBar = () => {
    if (this.userID()) {
      return <Row ><EmojiBar addNewEmoji={this.pushDisplayUserAddedEmoji} currentTime={this.getVideoTime} videoId={this.videoID()} emojiList={this.props.emojiList} userID={this.userID()} /></Row>
    } else {
      return '';
    }
  }

  public render() {

    if (isNaN(parseInt(this.props.match.params.id, 10))) {
      return <Redirect to="/" />
    }
    return (
      <div id="playSongBackgroundImage">
        <div>

          {
            this.state.displayUserAddedEmoji.map((emoji: any, index: any) => {
              return <Emoji key={index} Emoji={emoji.data} src={returnIcon(emoji.data.id)} />
            })
          }
        </div>
        <Container>
          {this.getVideoPlayer()}
          {/* <VideoPlayer ref={this.myRef} setCurrent={this.setVideoTime} src={`http://localhost:8080/OGSongs/Haim_TheWire.mp4`} /> */}
          {/* {
              this.props.songRecord.map((song:any)=>
                song
                // < SongDetail videoId={song.id} />
              )
              } */}
          {this.getSongDetail()}
          {this.getEmojiBar()}
        </Container>
      </div >

    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  emojiList: state.emojiList.emojiList,
  songRecord: state.songRecord.songRecord
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  viewSongRecord: (sonRecordID: any) => {
    dispatch(viewSongRecord(sonRecordID))
  }
  ,
  getEmojiList: () => {
    dispatch(getEmojiList())
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(ViewRecording);