import * as React from 'react';
import './SongDetail.css';
import returnIcon from 'src/components/Emoji'
import { Container, Row, Col } from 'reactstrap';


import { connect } from 'react-redux';
// import { viewSongRecord } from 'src/redux/viewRecording/actions';

import { ThunkDispatch, IRootState } from 'src/Store';
import { viewSongDetail } from 'src/redux/viewRecording/actions';
import { viewSongEmojiRecord } from 'src/redux/songRecordEmoji/actions';

class SongDetailBar extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }


  public componentDidMount() {
    this.props.viewSongDetail(this.props.videoId);
    this.props.viewSongEmojiRecord(this.props.videoId);
  }


  public componentDidUpdate(prevProps: any) {
    if (prevProps.videoId !== this.props.videoId) {
      this.props.viewSongDetail(this.props.videoId);
      this.props.viewSongEmojiRecord(this.props.videoId);
    }
  }

  public render() {
    const imgStyle: any = {
      width: '20px',
      height: '20px'
    };
    return (
      <Container>
        <Row>

          {
            this.props.selectedSong.map((song: any, index: any) =>
              <Container key={index} className="container">
                <Row style={{color: '#fff', fontSize: '1.4rem'}} >
                  Song: {song.song_name} <br />Genre: {song.genre}
                </Row>
                <Row >
                  {
                    this.props.emojiRecord.emojiList.map((emoji: any, index2: any) =>
                      <Col key={index2} style={{color: '#fff'}}><img style={imgStyle} src={returnIcon(emoji.id)} /> x {emoji.emojiCount}</Col>
                    )
                  }
                </Row>
                {/* <div>
                  singer_id:{song.singer_id}
                </div> */}
              </Container>)
          }
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  selectedSong: state.selectedSong.selectedSong,
  emojiRecord: state.songEmojiRecord.emojiRecords
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  viewSongDetail: (id: any) => {
    dispatch(viewSongDetail(id))
  },
  viewSongEmojiRecord: (id: any) => {
    dispatch(viewSongEmojiRecord(id));
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(SongDetailBar);