# Emojioke
## By Kevin Lai, Matthew Woodroffe
### Emojioke is a karaoke sharing platform with rating system through 5 emoji which represent 5 value. The work flow as follow:

1. User select song and redirect to song recording page
2. Once the user click start button, the song video will start playing and use Web Audio API to recording user voice
3. When stop button clicked, stop recording and use save the audio as mp3 file then upload to the Server side
4. Server side received the uploaded file then use FFMPEG to merge with corresonding song file to create a new mp4 file
5. In Hone page of Client side, render all of the user record and ranking by the scores for other people to view
6. When someone view a user record, they can add emoji on top to the record. The corresonding emoji and clicked time to the database through the server at the same time.
7. The emoji record use for calculate the scores of the each user record and re-render when the user record being view.

# Prerequisite
You should include these in .env file in Server side
DB_NAME=[database_name]
DB_USERNAME=[your_user]
DB_PASSWORD=[your_password]
JWT_SECRET=[some_jwt_srcret]

You should install yarn to run this project.
Then, yarn install in both server and react side after clone.
